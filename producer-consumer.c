/*
	myshell.c

	Chase Whitener
	FSUID: ccw09c
	CSID: whitener

	Project 3, Producer/Consumer
	COP4610 - Operating Systems
	FSU - Spring 2013
*/

/* Turn on GNU goodiness */
#define _GNU_SOURCE

/* includes */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <semaphore.h>
#include <string.h>
#include <pthread.h>

#include <buffer.h>

buffer_item BUFFER[BUFFER_SIZE];
int insert_ptr = 0;
int remove_ptr = 0;
pthread_mutex_t mutex;
sem_t empty;
sem_t full;

void *consumer( void *args );
int insert_item( buffer_item item );
int is_integer( const char *val );
void *producer( void *args );
int remove_item( buffer_item *item );

/* -----------------------------------------------------------------------------
 main( int argc, char** argv )
 -- control the flow of our application with a while loop
----------------------------------------------------------------------------- */
int main( int argc, char** argv ) {
	srand(time(0));
	pthread_mutex_init(&mutex, NULL);
	if ( sem_init( &full, 0, 0 ) ) {
		perror( "Semphore initialization failed.\n" );
		return -1;
	}
	if ( sem_init( &empty, 0, BUFFER_SIZE-1 ) ) {
		perror( "Semphore initialization failed.\n" );
		return -1;
	}
	int sleep_time = 0;
	int producer_threads = 0;
	int consumer_threads = 0;
	if ( 4 != argc ) {
		printf( "Usage: producer-consumer.x SLEEP_TIME PROD_THREADS CONS_THREADS\n" );
		exit(-1);
	}
	if ( 0==is_integer(argv[1]) || 0==is_integer(argv[2]) || 0==is_integer(argv[3]) ) {
		fprintf( stderr, "producer-consumer expects three positive integers.\n\n" );
		printf( "Usage: producer-consumer.x SLEEP_TIME PROD_THREADS CONS_THREADS\n" );
		exit(-1);
	}
	sleep_time = atoi( argv[1] );
	producer_threads = atoi( argv[2] );
	consumer_threads = atoi( argv[3] );

	for ( int i = 0; i < producer_threads; ++i ) {
		pthread_t tid;
		pthread_attr_t attr;
		pthread_attr_init( &attr );
		pthread_create( &tid, &attr, producer, NULL );
	}
	for ( int i = 0; i < consumer_threads; ++i ) {
		pthread_t tid;
		pthread_attr_t attr;
		pthread_attr_init( &attr );
		pthread_create( &tid, &attr, consumer, NULL );
	}
	sleep( sleep_time );
	sem_destroy( &full );
	sem_destroy( &empty );
	return 0;
}

/* -----------------------------------------------------------------------------
 consumer( void *args )
 -- handles callback for consumer threads
----------------------------------------------------------------------------- */
void *consumer( void *args ) {
	if ( NULL != args ) return NULL;
	while ( 1 ) {
		sleep( (int)(rand() % BUFFER_SIZE) );
		buffer_item random = -1;
		if ( remove_item( &random ) ) {
			fprintf( stderr, "Error Consuming\n" );
		}
		else {
			printf( "Comsumer consumed %d.\n", random );
		}
	}
	return NULL;
}

/* -----------------------------------------------------------------------------
 insert_item( buffer_item item )
 -- Add item to buffer. 0 on success, -1 on error
----------------------------------------------------------------------------- */
int insert_item( buffer_item item ) {
	if ( -1 == item ) return -1;
	sem_wait( &empty );
	pthread_mutex_lock(&mutex);
	BUFFER[insert_ptr++] = item;
	insert_ptr = insert_ptr % 5;
	pthread_mutex_unlock(&mutex);
	sem_post( &full );
	return 0;
}

/* -----------------------------------------------------------------------------
 is_integer( const char *val )
 -- 1 if is int, 0 otherwise
----------------------------------------------------------------------------- */
int is_integer( const char *val ) {
	if ( NULL == val ) return 0;
	if ( 0 == strlen(val) ) return 0;
	for ( size_t i = 0; i < strlen(val); ++i ) {
		switch( val[i] ) {
			case '0':
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
				continue;
			break;
			default:
				return 0;
		}
	}
	return 1;
}

/* -----------------------------------------------------------------------------
 producer( void *args )
 -- handles callback for producer threads
----------------------------------------------------------------------------- */
void *producer( void *args ) {
	if ( NULL != args ) return NULL;
	while( 1 ) {
		sleep( (int)(rand() % BUFFER_SIZE) );
		buffer_item random = rand();
		if ( insert_item( random ) ) {
			fprintf( stderr, "Producer Error\n" );
		}
		printf( "Producer produced %d.\n", random );
	}
	return NULL;
}

/* -----------------------------------------------------------------------------
 remove_item( buffer_item *item )
 -- Remove item from buffer. 0 on success, -1 on error
----------------------------------------------------------------------------- */
int remove_item( buffer_item *item ) {
	if ( NULL == item ) return -1;
	sem_wait( &full );
	pthread_mutex_lock(&mutex);
	*item = BUFFER[remove_ptr];
	BUFFER[remove_ptr++] = -1;
	remove_ptr = remove_ptr % 5;
	pthread_mutex_unlock(&mutex);
	sem_post( &empty );
	return 0;
}
