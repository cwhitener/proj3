/*
	myshell.c

	Chase Whitener
	FSUID: ccw09c
	CSID: whitener

	Project 3, producer consumer buffer
	COP4610 - Operating Systems
	FSU - Spring 2013
*/

#ifndef _BUFFER_H
#define _BUFFER_H

typedef int buffer_item;
#define BUFFER_SIZE 5

#endif

/*
produce
do {
	//produce an item in nextp
	wait(empty);
	wait(mutex);
	// add nextp to buffer
	signal(mutex);
	signal(full);
} while (TRUE);

consume
do {
	wait (full);
	wait (mutex);
	// remove an item from buffer to nextc
	signal(mutex);
	signal(empty);
	// consume the item in nextc
} while (TRUE);

*/