# make file for our project

all: matrix.x producer-consumer.x

matrix.x: ./matrix.c
	gcc47 -std=c11 -W -Wall -Wextra -pedantic -lpthread ./matrix.c -o matrix.x
producer-consumer.x: ./buffer.h ./producer-consumer.c
	gcc47 -std=c11 -W -Wall -Wextra -pedantic -lpthread -I. ./producer-consumer.c -o producer-consumer.x
clean:
	rm -f *.x
