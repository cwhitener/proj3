/*
	myshell.c

	Chase Whitener
	FSUID: ccw09c
	CSID: whitener

	Project 3, Matrix solver
	COP4610 - Operating Systems
	FSU - Spring 2013
*/

/* includes */
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#define M 3
#define K 2
#define N 3
#define NUM_THREADS 9

typedef struct {
	int i;
	int j;
} thread_parm_t;

int COUNT;
pthread_t WORKERS[NUM_THREADS];
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
int A[M][K] = { {1,4}, {2,5}, {3,6} };
int B[K][N] = { {8,7,6}, {5,4,3} };
int C[M][N] = { {0,0,0}, {0,0,0}, {0,0,0} };
int generate_threads( int i );
void dump_contents( void );
void *runner( void *param );

/* -----------------------------------------------------------------------------
 main( void )
 -- control the flow of our application with a while loop
----------------------------------------------------------------------------- */
int main( void ) {
	COUNT = 0;
	for (int i = 0; i < M; ++i ) {
		if ( 0 == generate_threads( i ) ) {
			break;
		}
	}

	for ( int i = 0; i < COUNT; ++i ) {
		if ( 0 != pthread_join( WORKERS[i], NULL ) ) {
			fprintf( stderr, "Tried to join an invalid thread %d\n", i );
		}
	}
	dump_contents();
	return 0;
}

/* -----------------------------------------------------------------------------
 dump_contents( void )
 -- dump the contents of our matrices!
----------------------------------------------------------------------------- */
void dump_contents( void ) {
	printf( "\nMatrix A:\n");
	for ( int i = 0; i < M; ++i ) {
		for ( int j = 0; j < K; ++j ) {
			if ( j > 0 ) printf(" ");
			printf("%d",A[i][j]);
		}
		printf("\n");
	}
	printf( "\nMatrix B:\n");
	for ( int i = 0; i < K; ++i ) {
		for ( int j = 0; j < N; ++j ) {
			if ( j > 0 ) printf(" ");
			printf("%d",B[i][j]);
		}
		printf("\n");
	}
	printf( "\nMatrix C = AB\n");
	for ( int i = 0; i < M; ++i ) {
		for ( int j = 0; j < N; ++j ) {
			if ( j > 0 ) printf(" ");
			printf("%d",C[i][j]);
		}
		printf("\n");
	}
	printf("\n");
}

/* -----------------------------------------------------------------------------
 generate_threads( int i )
 -- start us up a thread and handle errors!
----------------------------------------------------------------------------- */
int generate_threads( int i ) {
	if ( COUNT >= NUM_THREADS ) {
		fprintf( stderr, "Tried to generate too many threads" );
		return 0;
	}
	if ( i < 0 || i >= M ) return 0;
	for ( int j = 0; j < N; ++j ) {
		thread_parm_t *data = NULL;
		if ( NULL == (data=malloc(sizeof(thread_parm_t)) ) ) {
			fprintf( stderr, "Error allocating space\n" );
			return 0;
		}
		data->i = i;
		data->j = j;

		if ( 0 != pthread_create( &WORKERS[COUNT], NULL, &runner, (void *)data ) ) {
			fprintf( stderr, "Can't create thread: %d\n", COUNT );
			return 0;
		}
		COUNT++;
	}
	return 1;
}

/* -----------------------------------------------------------------------------
 runner( void *param )
 -- thread running callback
----------------------------------------------------------------------------- */
void *runner( void *ptr ) {
	if ( NULL == ptr ) return NULL;
	//pthread_mutex_lock( &mutex );
	thread_parm_t *p = (thread_parm_t *)ptr;
	for ( int n = 0; n < K; ++n ) {
		C[p->i][p->j] += A[p->i][n] * B[n][p->j];
	}
	if (NULL != p ) free(p);
	//pthread_mutex_unlock( &mutex );
	return NULL;
}
